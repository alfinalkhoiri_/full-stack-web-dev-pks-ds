<?php
trait Hewan {
  public $nama;
  public $darah;
  public $jumlahKaki;
  public $keahlian;

  public function setjenis($nama, $darah = 50, $jumlahKaki, $keahlian) {
    $this->nama = $nama; 
    $this->darah = $darah;
    $this->jumlahKaki = $jumlahKaki;
    $this->keahlian = $keahlian;
  }
  public function getjenis() {
    echo "Nama Hewan : $this->nama <br> Darah : $this->darah <br> Jumlah Kaki : $this->jumlahKaki <br> Keahlian : $this->keahlian <br>";
  }

  public function atraksi(){
      echo "Hewan $this->nama sedang  $this->keahlian <br>";
  }
}

trait Fight {
  public  $attackPower,
          $defencePower;
  public function kekuatan($attackPower, $defencePower){
    $this->attackPower = $attackPower; 
    $this->defencePower = $defencePower;
}
  public function ambilkekuatan() {
    echo " attackPower : $this->attackPower <br> defencePower : $this->defencePower <br>";
  }

  public function serang(){
    echo "Hewan $this->nama sedang menyerang $this->nama <br><br>";
  }

  public function diserang(){
    echo "Hewan $this->nama sedang menyerang $this->nama <br><br>";
  }
}

class Elang {
  use Hewan;
  use Fight;
public function InfoHewan(){
  $this->attackPower = $attackPower; 
  $this->defencePower = $defencePower;
}
public function getInfoHewan(){
    echo "Nama Hewan : $this->nama <br> Darah : $this->darah <br> Jumlah Kaki : $this->jumlahKaki <br> Keahlian : $this->keahlian <br> attackPower : $this->attackPower <br> defencePower : $this->defencePower <br>";
  }

}



//Elang
$elang = new Elang();
$elang->setjenis("Elang", 50, 2, "Terbang tinggi");
echo $elang->getjenis();

$elang2 = new Elang();
$elang2->kekuatan(10, 5);
echo $elang2->ambilkekuatan();
echo "<br>";
echo $elang->atraksi();
echo $elang->serang();
echo $elang->getInfoHewan();
echo "<br>";




//Harimau
class Harimau {
  use Hewan;
  use Fight;

  public function getInfoHewan(){
    echo "Nama Hewan : $this->nama <br> Darah : $this->darah <br> Jumlah Kaki : $this->jumlahKaki <br> Keahlian : $this->keahlian <br> attackPower : $this->attackPower <br> defencePower : $this->defencePower <br>";
  }
}
$harimau = new Harimau();
$harimau->setjenis("Harimau", 50, 4, "Lari Cepat");
echo $harimau->getjenis();

$harimau2 = new Harimau();
$harimau2->kekuatan(10, 5);
echo $harimau2->ambilkekuatan();
echo "<br>";
echo $harimau->atraksi();
echo $harimau->serang();
echo $harimau->getInfoHewan();
echo "<br>";
?>